========
galgebro
========

Geometric algebra for Racket.


Usage
=====

So far *galgebro* only supports the *real linear space* of
*multi-elements* (also known as *multi-vectors*)

.. code:: racket

   > (require "galgebro/v00/multi-element.rkt")
   > (define-base e)

.. code:: text

   pᵣ = 1 e₁ + 1 e₂
   pₗ = 1 e₁ - 1 e₂

.. .. math::
   \underset{1}{p_r}
   &= 1\underset{1}{e} + 1\underset{2}{e}
   \\
   \underset{1}{p_l}
   &= 1\underset{1}{e} - 1\underset{2}{e}

.. code:: racket

   > (define pr (add (e 1 1) (e 2 +1)))
   '(add (e 1 1) (e 2 1))

   > (define pl (add (e 1 1) (e 2 -1)))
   '(add (e 1 1) (e 2 -1))

This means that only addition (subtraction)

.. code:: text

   pᵣ + pₗ = 2 e₁
   pᵣ - pₗ = 2 e₂

.. .. math::
   \underset{1}{p_r} + \underset{1}{p_l}
   &= 2\underset{1}{e}
   \\
   \underset{1}{p_r} - \underset{1}{p_l}
   &= 2\underset{2}{e}

.. code:: racket

   > (add pr pl)
   '(add (e 1 2))

   > (sub pr pl)
   '(add (e 2 2))

weight multiplication (known as scalar multiplication)

.. code:: text

   3 pₗ = 3 e₁ - 3 e₂

.. .. math::
   3 \underset{1}{p_l}
   = 3\underset{1}{e} - 3\underset{2}{e}

.. code:: racket

   > (mul 3 pl)
   '(add (e 1 3) (e 2 -3))

and ratios of **similar** multi-element are allowed

.. code:: text

   3 pₗ   3 e₁ - 3 e₂
   ──── = ─────────── = 3
    pₗ    1 e₁ - 1 e₂

.. .. math::
   \frac{ 3\underset{1}{p_l} }{ \underset{1}{p_l} }
   = \frac{
      3\underset{1}{e} - 3\underset{2}{e}
   }{
      1\underset{1}{e} - 1\underset{2}{e}
   }
   = 3

.. code:: racket

   > (div (mul 3 pl) pl)
   3

The Grassmann (exterior) algebra is under implementation.
This will be followed by the Grassmann bi-algebra and
ultimately by the Clifford algebra.


Installation
============

So far *galgebro* is not a proper package and is also
not present in <https://pkgs.racket-lang.org>.

Therefore the only way to use it right now is by copying
the directory `/lib/galgebro/` from the repository

.. code:: console

   racket-galgebro (repo)
   ├── all/
   ├── doc/
   ├── lib/
   │   ├── galgebro/  <=== copy this in your working DIR
   │   └── info.rkt
   ├── tst/
   ├── gitignore
   ├── Makefile
   └── README.rst

inside your working directory

.. code:: console

   your-working-directory/
   ├── galgebro/      <=== place *galgebro* here
   └── your-script.rkt

and then `require` what is implemented of *galgebro* from
`your-script.rkt`

.. code:: racket
   :number-lines:

   #lang racket/base

   (require "galgebro/v00/multi-elements.rkt")

   (define-base e)

   (define pr (add (e 1 1) (e 2 +1)))
   (define pl (add (e 1 1) (e 2 -1)))

   (add pr pl)
   (sub pr pl)

   (mul 3 pl)
   (div (mul 3 pl) pl)
