delfs = "*~"
delds = "compiled"

.PHONY: default
default:

.PHONY: cleaning
cleaning:
	for f in $(delfs); do \
	  find . -type f -name $$f -print -delete ; done
	for d in $(delds); do \
	  find . -type d -name $$d -exec rm -rv {} +; done
